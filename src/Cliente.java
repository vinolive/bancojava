
public class Cliente {
	private String nome;
	private String cpf;
	private String rg;
	private String endereco;
	private Conta conta;
	
	
	
	public Cliente(String nome, String cpf, String rg, String endereco, Conta conta) {
		this.nome = nome;
		this.cpf = cpf;
		this.rg = rg;
		this.endereco = endereco;
		this.conta = conta;
	}

	//metodos
	private boolean validaCpf(String valor) {
		if(valor!="") {
			this.cpf=valor;
			return true;
		}
		return false;
	}	
	
	//getters e setters
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		if(this.validaCpf(cpf)) {
			this.cpf = cpf;			
		}else {
			this.cpf = "0000000000";
		}
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	
}
