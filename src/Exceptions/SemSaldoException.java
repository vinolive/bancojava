package Exceptions;

public class SemSaldoException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SemSaldoException(String msn) {
		super(msn); 
	}
}
