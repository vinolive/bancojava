import java.util.ArrayList;

import Exceptions.ClienteNaoEncontradoException;
import Exceptions.SemSaldoException;

import java.util.*;

public class App {
	public static void main(String[] args) {
		int contaCont = 0;
		Scanner teclado = new Scanner(System.in);
		List<Cliente>lista = new ArrayList<Cliente>();
		Banco banco = new Banco("Nubanco","0988833344411",2334,lista);
		Conta conta1 = new Conta(contaCont++,9000,9100,1,123456,banco);
		Conta conta2 = new Conta(contaCont++,10000,19100,1,123456,banco);
		Cliente pedro = new Cliente("Pedro","100000000000","123123123","Rua dos Alfeneiros",conta1);
		Cliente luiza = new Cliente("Luiza","100000000123","123123123","Rua dos Alfeneiros",conta2);
		banco.adicionaCliente(luiza);
		banco.adicionaCliente(pedro);
		menu(banco,teclado);
}
	
	public static void menu(Banco banco,Scanner teclado){
		int opcao = -1;
		while(opcao!=0){
			opcao = opcoes(teclado);
			switch (opcao) {
			case 1:
				Cliente cliente = criarConta(opcao, teclado, banco);
				banco.adicionaCliente(cliente);
				clearBuffer(teclado);
				break;
			case 2:
				int escolhidoSacar=-1;
				try {
					 escolhidoSacar= banco.retornaCliente(teclado);		
					 System.out.println("Valor: ");
					 float valor = teclado.nextFloat();
					 banco.getClientes().get(escolhidoSacar).getConta().sacar(valor);
					 System.out.println(banco.getClientes().get(escolhidoSacar).getConta().getSaldo());
				}catch (ClienteNaoEncontradoException e) {
					System.out.println("Erro: " + e.getMessage());				
				}catch (SemSaldoException e) {
					System.out.println("Erro: " + e.getMessage());
				}
				
				
				break;		
			case 3:
				int escolhidoDepositar=-1;
				try {
					escolhidoDepositar= banco.retornaCliente(teclado);		
					 System.out.println("Valor: ");
					 float valor = teclado.nextFloat();
					 banco.getClientes().get(escolhidoDepositar).getConta().sacar(valor);
					 System.out.println(banco.getClientes().get(escolhidoDepositar).getConta().getSaldo());
				}catch (ClienteNaoEncontradoException e) {
					System.out.println("N�o foi possivel encontrar cliente: " + e.getMessage());				
				}catch (SemSaldoException e) {
					System.out.println("Saldo inferior ao saque: " + e.getMessage());
				}
			break;	
			default:
				teclado.close();
				System.out.println("Encerrando Programa!");
				break;
			}
		}
	}
	public static int opcoes(Scanner scan) {
		int opcao =-1;
		System.out.println("Escolha uma opcao: ");
		System.out.println("1 - Abrir Conta\n2 - Sacar\n3 - Depositar\n0 - Sair");
		try {
			opcao = scan.nextInt();
		}catch(InputMismatchException ex){
			System.out.println("Opcao Invalida!!!");
			return 0;
		}
		
		return opcao;
	}
	public static Cliente criarConta(int contaNum,Scanner teclado,Banco banco) {
		System.out.print("Nome: ");
		String nome = teclado.next();
		System.out.print("\nInsira seu cpf: ");
		String cpf = teclado.next();
		System.out.print("\nInsira seu rg: ");
		String rg = teclado.next();
		System.out.print("\nInsira sua senha: ");
		int senha = teclado.nextInt();
		System.out.print("Endereco: ");
		String endereco = teclado.next();
		Conta conta = new Conta(contaNum,1000,1000,1,senha,banco);
		Cliente cliente = new Cliente(nome,cpf,rg,endereco,conta);
		return cliente;
	}
	private static void clearBuffer(Scanner scanner) {
        if (scanner.hasNextLine()) {
            scanner.nextLine();
        }
    }
}
