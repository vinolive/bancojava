import static org.junit.jupiter.api.Assertions.*;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class ContaTeste {
	
	@Test
	public void sacarDeveRetornarZero() {
		int contaCont = 0;
		List<Cliente>lista = new ArrayList<Cliente>();
		Banco banco = new Banco("Nubanco","0988833344411",2334,lista);
		Conta conta1 = new Conta(contaCont++,1000,9100,1,123456,banco);
		Conta conta2 = new Conta(contaCont++,1000,19100,1,123456,banco);
		Cliente pedro = new Cliente("Pedro","100000000000","123123123","Rua dos Alfeneiros",conta1);
		Cliente luiza = new Cliente("Luiza","100000000123","123123123","Rua dos Alfeneiros",conta2);
		banco.adicionaCliente(luiza);
		banco.adicionaCliente(pedro);
		conta1.sacar(1000);
		assertEquals(0,conta1.getSaldo());
	}
	@Test
	public void sacarDeveRetornarDoisMil() {
		int contaCont = 0;
		List<Cliente>lista = new ArrayList<Cliente>();
		Banco banco = new Banco("Nubanco","0988833344411",2334,lista);
		Conta conta1 = new Conta(contaCont++,1000,9100,1,123456,banco);
		Conta conta2 = new Conta(contaCont++,1000,19100,1,123456,banco);
		Cliente pedro = new Cliente("Pedro","100000000000","123123123","Rua dos Alfeneiros",conta1);
		Cliente luiza = new Cliente("Luiza","100000000123","123123123","Rua dos Alfeneiros",conta2);
		banco.adicionaCliente(luiza);
		banco.adicionaCliente(pedro);
		conta1.depoistar(1000);
		assertEquals(2000,conta1.getSaldo());
	}
	
	@Test
	public void testAssertArrayEquals() {
		byte[] esperado = "teste".getBytes();
		byte[] result = "teste".getBytes();
		assertArrayEquals(esperado, result);
	}
	
	@Test
	public void testAssertFalse() {
		assertFalse(false);
	}
	
	@Test
	public void testAssertObj() {
		assertNotEquals(new Object(), new Object());
	}
	
	//Rules
	@Rule
	public TemporaryFolder tmpFolder = new TemporaryFolder();
	
	@Test 
	public void deveriaCriarArquivo() throws IOException {
		File arquivoCriado = tmpFolder.newFile("file.txt");
		assertTrue(arquivoCriado.isFile());
		assertEquals(tmpFolder.getRoot(), arquivoCriado.getParentFile());
	}
}
