import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Exceptions.ClienteNaoEncontradoException;

public class Banco {
	private String nome;
	private String cnpj;
	private int agencia;
	private List<Cliente> clientes = new ArrayList<Cliente>();
	
	
	
	public Banco(String nome, String cnpj, int agencia, List<Cliente> clientes) {
		this.nome = nome;
		this.cnpj = cnpj;
		this.agencia = agencia;
		this.clientes = clientes;
	}

	//metodos
	/**
	 * 
	 * @param cliente
	 */
	public void adicionaCliente(Cliente cliente) {
		clientes.add(cliente);
	}
	
	public void imprime() {
		int i=1;
		for(Cliente a : clientes) {
			System.out.println(i +" - " +a.getNome());
			i++;
		}
	}
	
	/**
	 * 
	 * @param cpf
	 * @return o index do cliente na lista
	 */
	public int retornaCliente(Scanner teclado) throws ClienteNaoEncontradoException{
		int i=-1;
		boolean achou=false;
		System.out.println("Cpf do cliente: ");
		String cpf=teclado.next();
		for(Cliente a : clientes) {
			if(a.getCpf().equals(cpf)) {
				return i;
			}
			i++;
		}
		if(achou==false) {
			throw new ClienteNaoEncontradoException("Cliente n�o encontrado!");
		}
		return -1;
	}
	
	//getters e setters
	public String getNome(){
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public int getAgencia() {
		return agencia;
	}
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	public List<Cliente> getClientes() {
		return clientes;
	}
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	
	
}
