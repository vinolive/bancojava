import Exceptions.SemSaldoException;

public class Conta {
	private int numConta;
	private double saldo;
	private double limite;
	private int tipo;
	private int senha;
	private Banco banco;
	
	/**
	 * @param numConta
	 * @param saldo
	 * @param limite
	 * @param tipo
	 * @param senha
	 * @param banco
	 */
	
	public Conta(int numConta, double saldo, double limite, int tipo, int senha, Banco banco) {
		this.numConta = numConta;
		this.saldo = saldo;
		this.limite = limite;
		this.tipo = tipo;
		this.senha = senha;
		this.banco = banco;
	}
	//metodos
	/**
	 * 
	 * @param valor
	 */
	public void sacar (double valor) throws SemSaldoException {
		if(valor>this.saldo) {
			throw new SemSaldoException("Saldo insuficiente");
		}
		this.saldo-=valor;
	}
	public void depoistar(double valor) {
		this.saldo+=valor;
	}
	
	//getters and setters
	public int getNumConta() {
		return numConta;
	}
	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getLimite() {
		return limite;
	}
	public void setLimite(double limite) {
		this.limite = limite;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getSenha() {
		return senha;
	}
	public void setSenha(int senha) {
		this.senha = senha;
	}
	public Banco getBanco() {
		return banco;
	}
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	
	
}
